import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpClient: HttpClient) { }

  public sendGetRequest(url: string) : Observable<any> {
    return this.httpClient.get(url);
  }


  public sendPostRequest(url: string,  body = {}) : Observable<any> {
    return this.httpClient.post(url, body);
  }
}

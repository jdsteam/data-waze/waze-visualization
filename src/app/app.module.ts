import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatNativeDateModule, MatFormFieldModule,} from '@angular/material';
import { MatDatepickerModule} from '@angular/material/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSliderModule} from '@angular/material/slider';
import { MatRadioModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

import { MapComponent } from './map/map.component';
import { MapFloodComponent } from './map-flood/map-flood.component';

import { Select2Module } from 'ng2-select2';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {NgbModule, NgbInputDatepicker} from '@ng-bootstrap/ng-bootstrap';
 import {DpDatePickerModule} from 'ng2-date-picker';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { OwlDateTimeModule} from 'ng-pick-datetime';
import { OwlMomentDateTimeModule } from 'ng-pick-datetime-moment';
import { OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NgxLoadingModule } from 'ngx-loading'; 
import { OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';

import { AppRoutingModule } from './app-routing.module';
import {DatePipe} from '@angular/common';



@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    MapFloodComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRoutingModule),
    HttpClientModule,
    NgxMapboxGLModule.withConfig({
      accessToken: 'pk.eyJ1IjoieXVudXNmZnoiLCJhIjoiY2sxaDk2M244MDUwaTNwcmw0cW1rdGUwdCJ9.g7TWx7Neha1XrlEAUBubTQ', // Optionnal, can also be set per map (accessToken input of mgl-map)
      geocoderAccessToken: 'TOKEN' // Optionnal, specify if different from the map access token, can also be set per mgl-geocoder (accessToken input of mgl-geocoder)
    }),
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatRadioModule,
    FormsModule,
    Select2Module,
    MatSelectModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatInputModule,
    NgMultiSelectDropDownModule,
    NgbModule,
    DpDatePickerModule,
    NgxMaterialTimepickerModule,
    OwlDateTimeModule, OwlMomentDateTimeModule, OwlNativeDateTimeModule,
    NgxLoadingModule
  ],
  providers: [
    DatePipe,

    MatDatepickerModule,
    // use french locale
    {provide: OWL_DATE_TIME_LOCALE, useValue: 'id'},
  ],
  bootstrap: [AppComponent]

})
export class AppModule { }

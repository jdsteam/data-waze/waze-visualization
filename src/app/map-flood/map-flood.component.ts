import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Map, Popup } from 'mapbox-gl';
import * as MapboxDraw from '@mapbox/mapbox-gl-draw';

import { DataService } from '../data.service';
import jsonCity from "./../../assets/json/cities.json";
import { MomentDateTimeAdapter, OWL_MOMENT_DATE_TIME_FORMATS } from 'ng-pick-datetime-moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';

import { DatePipe } from '@angular/common';
import * as _moment from 'moment';

const moment = (_moment as any).default ? (_moment as any).default : _moment;

@Component({
  selector: 'app-map',
  templateUrl: './map-flood.component.html',
  styleUrls: ['./map-flood.component.css'],
  providers: [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: OWL_MOMENT_DATE_TIME_FORMATS},
    {provide: OWL_DATE_TIME_LOCALE, useValue: 'id'}
  ]

})
export class MapFloodComponent implements OnInit {
  map: Map;

  mapConfig : {
    zoom: object,
    center: object
  };

  draw = new MapboxDraw({
    displayControlsDefault: false,
    controls: {
    polygon: true,
    trash: true
    }
  });

  // set default basemap
  layerId = 'light';

  // style basemap
  mapStyle: string;

  // polygon
  itemPolygon : any;

  // area kota prov jabar
  cities : {
    geometry : object,
    properties : object
  }[] = [];

  // show toggle basemap
  isShowToggleBasemap = true;

  // point lokasi banjir
  dataFlood : any;
  sourceFlood : any;


  // set default date
  defaultDateSt = new moment(new Date(new Date().setHours(8,0,0,0)));
  defaultDateNd = new moment(new Date(new Date().setHours(8,1,0,0)));


  // filter date
  startDate : string;
  endDate : string;

  // popup
  popupPoint: any
  popupData: any

   // Tipe alert dan Icon
   alertType = [
    // {id: 'HAZARD_WEATHER_HAIL', icon : 'hail.png'},
    {id: 'HAZARD_WEATHER_FLOOD', icon : 'flood'},
    {id: 'HAZARD_WEATHER_HEAVY_RAIN', icon : 'heavyrain'},
    {id: 'HAZARD_WEATHER_HAIL', icon : 'hail'},
  ]

  // Show/Hide Legend
  isShowLegend : string

  // live update
  isLive = false
  liveInterval : any

  date = new FormControl({ disabled: true, value: '' })


  constructor(
    private dataService: DataService,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.mapConfig = {
      zoom: [8],
      center : [107.594869, -6.952371]
    };

    this.changeStyle(this.layerId);


    // init draw city
    jsonCity['features'].forEach(element => {
      this.cities.push({
        geometry: element.geometry,
        properties: element.properties
      });
    });

    // draw point flood
    this.dataFlood = []

  }

  onLoadMap(event) {
    this.map = event;
    this.map.addControl(this.draw, 'top-right');


    this.map.on('draw.delete', () => {
      var data = this.draw.getAll();
      this.itemPolygon = data;
    });

    this.map.loadImage(
      'assets/images/flood.png',
      (error, image) => {

        if (error) throw error;
        this.map.addImage('flood', image);
      }
    );

    this.map.loadImage(
      'assets/images/hail.png',
      (error, image) => {

        if (error) throw error;
        this.map.addImage('hail', image);
      }
    );

    this.map.loadImage(
      'assets/images/heavy-rain.png',
      (error, image) => {

        if (error) throw error;
        this.map.addImage('heavyrain', image);
      }
    );

    // move titik banjir to top layer
    let layerTitikBanjir = this.map.getLayer('titikBanjir');
    if(typeof layerTitikBanjir !== 'undefined') {
      this.map.moveLayer('titikBanjir');
    }

    this.map.on('click', 'titikBanjir', (e) => {
      this.popupPoint = e.features[0].geometry['coordinates'].slice();
      let popupData = e.features[0].properties;

      // Ensure that if the map is zoomed out such that multiple
      // copies of the feature are visible, the popup appears
      // over the copy being pointed to.
      while (Math.abs(e.lngLat.lng - this.popupPoint[0]) > 180) {
        this.popupPoint[0] += e.lngLat.lng > this.popupPoint[0] ? 360 : -360;
      }

      console.log(this.popupData)

      new Popup()
      .setLngLat(this.popupPoint)
      .setHTML(`
      <table width="100%">
      <tr>
        <th style="padding:3px;">Type</th>
        <td style="padding:3px;">:</td>
        <td style="padding:3px;color:#008C50;">${popupData.type}</td>
      </tr>
      <tr>
        <th style="padding:3px;">Sub Type</th>
        <td style="padding:3px;">:</td>
        <td style="padding:3px;color:#008C50;">${popupData.subtype}</td>
      </tr>
      <tr>
        <th style="padding:3px;">Location</th>
        <td style="padding:3px;">:</td>
        <td style="padding:3px;color:#008C50;">${popupData.location}</td>
      </tr>
      <tr>
        <th style="padding:3px;">Created Time</th>
        <td style="padding:3px;">:</td>
        <td style="padding:3px;color:#008C50;">${popupData.created_time}</td>
      </tr>
    </table>
      `)
      .addTo(this.map);

    });

    // load titik banjir
    setTimeout(() => {
      this.isLive = true
    }, 1000)
    this.drawTitikBanjir(this.startDate, this.endDate);
    setInterval(() => {
      this.drawTitikBanjir(this.startDate, this.endDate);
    }, 180000)
  }

  //  change base map
  toggleDisplayBasemap() {
    this.isShowToggleBasemap = !this.isShowToggleBasemap;
  }

  changeStyle(layerId: string) {
    this.mapStyle = `mapbox://styles/mapbox/${layerId}-v9`;
    this.layerId  = layerId;
  }

  drawTitikBanjir(startDate = '', endDate = '') {
    this.clearTitikBanjir()
    let url = "http://waze-api.digitalservice.id/banjir/?start_time=" + startDate + "&end_time=" + endDate
    let i = 0;
    this.dataService.sendGetRequest(url).subscribe(
      res => {
        res.data.forEach(data => {
          i++;
          this.dataFlood.push({
            properties : {
              'type' : data.type,
              'subtype' : data.subtype,
              'location' : data.location,
              'created_time' : data.created_time
            },
            geometry: {
              'type' : 'Point',
              'coordinates': data.location,

            }
          })
        })

        this.sourceFlood = {
          type : 'geojson',
          data : {
            type : "FeatureCollection",
            features : this.dataFlood
          }
        }

        this.map.addSource('titikBanjirSource', this.sourceFlood);

        // add layer
        this.map.addLayer({
         id: 'titikBanjir',
         type: 'symbol',
         source: this.sourceFlood,
         layout: {
          'icon-image': 'flood',
          'icon-size': 1
         }
        });
        console.log(i);
      }
    )

  }

  clearTitikBanjir() {
    this.dataFlood = [];
     // clear data
     let mapLayer = this.map.getLayer('titikBanjir');
     let mapSource = this.map.getSource('titikBanjirSource');

     if(typeof mapLayer !== 'undefined') {
       // Remove map layer & source.
       this.map.removeLayer('titikBanjir');
       this.map.removeSource('titikBanjir');
     }

    if(typeof mapSource !== 'undefined') {
      this.map.removeSource('titikBanjirSource');
    }
     // end clear data
  }

  changeDate(id) {
    if(id == 'start') {
      this.startDate = this.datePipe.transform(new Date(this.defaultDateSt), "y-MM-ddTHH:mm:ss");
      this.defaultDateNd = moment(this.defaultDateSt, "YYYY-MM-DDThh:mm:ss A").add(1, 'minutes')
      this.endDate = this.datePipe.transform(new Date(this.defaultDateNd), "y-MM-ddTHH:mm:ss");
    }
    else {
      console.log(this.defaultDateNd)
      this.endDate = this.datePipe.transform(new Date(this.defaultDateNd), "y-MM-ddTHH:mm:ss");
    }
  }

  toggleLive(event) {
    this.isLive = event.checked;
    console.log(this.isLive)
    if(this.isLive) {
      this.drawTitikBanjir(this.startDate, this.endDate);
      this.liveInterval = setInterval(() => {
        this.drawTitikBanjir(this.startDate, this.endDate);
      }, 180000)
    } else {
      clearInterval(this.liveInterval)
      this.drawTitikBanjir(this.startDate, this.endDate)
    }
  }

  onSubmit() {

    if(this.isLive) {
      this.drawTitikBanjir(this.startDate, this.endDate);
      this.liveInterval = setInterval(() => {
        this.drawTitikBanjir(this.startDate, this.endDate);
      }, 180000)
    } else {
      clearInterval(this.liveInterval)
      this.drawTitikBanjir(this.startDate, this.endDate)
    }
  }


}

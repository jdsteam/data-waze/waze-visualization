import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: '<div style="position:absolute; top:0; left: 0;width:100%;height:100%"><router-outlet></router-outlet></div>'
})
export class AppComponent {
  title = 'mapbox-angular';
}

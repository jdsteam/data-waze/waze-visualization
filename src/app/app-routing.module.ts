import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapComponent } from './map/map.component';
import { MapFloodComponent } from './map-flood/map-flood.component';

export const AppRoutingModule: Routes = [
  {
    path: '',
    component: MapComponent
  },
  {
    path: 'flood',
    component: MapFloodComponent
  },
];
